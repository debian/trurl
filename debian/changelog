trurl (0.16-1) unstable; urgency=medium

  * New upstream version 0.16

 -- Michael Ablassmeier <abi@debian.org>  Thu, 19 Sep 2024 09:44:20 +0200

trurl (0.15.1-1) unstable; urgency=medium

  [ Michael Ablassmeier ]
  * New upstream version 0.15.1
  * debian/patches: remove patch for manpage
  * debian/watch: use release packages instead of git repo which doesn't
    contain manpage
  * debian/watch: fix version detect

  [ Carlos Henrique Lima Melara ]
  * debian/copyright: update upstream copyright stanza.

 -- Michael Ablassmeier <abi@debian.org>  Sun, 15 Sep 2024 09:15:19 +0200

trurl (0.15-1) UNRELEASED; urgency=medium

  * New upstream version 0.15
  * Add debian/patches/manpage: ship manpage in nroff format

 -- Michael Ablassmeier <abi@debian.org>  Wed, 28 Aug 2024 09:22:36 +0200

trurl (0.14-1) unstable; urgency=medium

  * New upstream version 0.14
  * Raise standards version to 4.7.0

 -- Michael Ablassmeier <abi@debian.org>  Mon, 29 Jul 2024 21:33:29 +0200

trurl (0.13-1) unstable; urgency=medium

  * New upstream version 0.13

 -- Michael Ablassmeier <abi@debian.org>  Wed, 15 May 2024 13:18:45 +0200

trurl (0.12-1) unstable; urgency=medium

  [ Michael Ablassmeier ]
  * New upstream version 0.12.
  * New upstream version 0.11.

  [ Carlos Henrique Lima Melara ]
  * debian/upstream/signing-key.asc: add upstream pgp key.
  * debian/watch: use git mode to get and verify signed tags.

 -- Carlos Henrique Lima Melara <charlesmelara@riseup.net>  Fri, 19 Apr 2024 00:52:23 -0300

trurl (0.10-1) unstable; urgency=medium

  [ Carlos Henrique Lima Melara ]
  * d/copyright: update copyright holders info and years.

  [ Michael Ablassmeier ]
  * New upstream version 0.10.

 -- Carlos Henrique Lima Melara <charlesmelara@riseup.net>  Wed, 28 Feb 2024 23:30:44 -0300

trurl (0.9-1) unstable; urgency=medium

  * New upstream version 0.9
  * debian/patches/versionfix: upstream forgot to raise version
  prior to release: add patch so executable contains correct
  version.

 -- Michael Ablassmeier <abi@debian.org>  Thu, 02 Nov 2023 12:16:49 +0100

trurl (0.8-2) unstable; urgency=medium

  * Modify testcases to be compatible with libcurl 8.3.0 (Closes: #1052828)

 -- Michael Ablassmeier <abi@debian.org>  Wed, 27 Sep 2023 20:16:36 +0200

trurl (0.8-1) unstable; urgency=medium

  [ Carlos Henrique Lima Melara ]
  * Run wrap-and-sort.
  * debian/control:
      - Add Vcs fields to source stanza.
      - Add myself as uploader.
      - Move to web section of the archive.
      - Remove python3-packaging from Build-Depends.
      - Slight rephrase of long description.
  * debian/copyright:
      - Add Upstream-Contact field.
      - Update packaging copyright section.
  * debian/gbp.conf: add basic definitions.
  * debian/rules:
      - Export DEB_BUILD_MAINT_OPTIONS.
      - Simplify rules exporting PREFIX.
  * debian/salsa-ci.yml: use salsa-ci.
  * debian/tests/control: add tests from upstream to autopkgtest.
  * debian/watch: scan via github API.

  [ Michael Ablassmeier ]
  * New upstream version 0.8.

 -- Carlos Henrique Lima Melara <charlesmelara@riseup.net>  Thu, 22 Jun 2023 09:59:32 -0300

trurl (0.7-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Ablassmeier <abi@debian.org>  Tue, 30 May 2023 21:28:56 +0200

trurl (0.6-1) unstable; urgency=medium

  * New upstream release.
  * Upstream moved tests from perl to python, add required
    build dependencies.
  * Add debian/trurl.docs: ship README and RELEASE-NOTES
  * Add debian/upstream/metadata

 -- Michael Ablassmeier <abi@debian.org>  Sat, 29 Apr 2023 07:57:37 +0200

trurl (0.4-1) unstable; urgency=medium

  * Initial release. (Closes: #1034375)

 -- Michael Ablassmeier <abi@debian.org>  Thu, 13 Apr 2023 21:32:08 +0200
